package com.ttx.test.app.mapper;

import com.ttx.test.app.entity.Account;
import static com.ttx.test.app.instance.AccountFactory.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

/**
 * @author TimFruit
 * @date 19-11-17 下午5:41
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//加事务, 在单元测试中默认回滚测试数据, 各个测试方法互不影响
// https://blog.csdn.net/qq_36781505/article/details/85339640
@Transactional
public class AccountMapperTests {
    @SpyBean
    AccountMapper accountMapper;


    @Test
    public void shouldSelectAccount(){

        //given 测试数据库中的数据

        //when
        Account result=accountMapper.selectAccount("ttx");

        //then
        Assert.assertEquals("ttx@yourdomain.com", result.getEmail());

    }

    @Test
    public void shouldInsertAccount(){
        //given
        Account timAccount=createTimFruitAccount();
        //when
        accountMapper.insertAccount(timAccount);


        //then
        Account result=accountMapper.selectAccount(timfruitUserId);
        Assert.assertEquals(timfruitUserId, result.getUserid());
        Assert.assertEquals(timAccount.getCity(), result.getCity());
        Assert.assertEquals(timAccount.getAddr1(), result.getAddr1());
        Assert.assertEquals(timAccount.getAddr2(), result.getAddr2());
    }


    @Test
    public void shouldUpdateCountry(){
        //given
        String userId="ttx";
        String country="万兽之国";

        //when
        accountMapper.updateCountryByUserId(userId, country);

        //then
        Account result=accountMapper.selectAccount(userId);
        Assert.assertEquals(country, result.getCountry());

    }

    @Test
    public void shouldDeleteAccount(){
        //given
        String userId="ttx";
        Account account=accountMapper.selectAccount(userId);
        Assert.assertTrue(account!=null);



        //when
        accountMapper.deleteAccountry(userId);


        //then
        account=accountMapper.selectAccount(userId);
        Assert.assertTrue(account==null);
    }

    @Test
    public void shouldSaveAccountBatch(){

        //given
        //update
        String userId1="ttx";
        String country1="天堂";
        Account account1=accountMapper.selectAccount(userId1);
        account1.setCountry(country1);
        //insert
        String userId2=timfruitUserId;
        String country2="中国";
        Account account2=createTimFruitAccount();
        account2.setCountry(country2);

        List<Account> accountList=Arrays.asList(account1,account2);

        //when
        accountMapper.saveAccountBatch(accountList);

        //then
        account1=accountMapper.selectAccount(userId1);
        Assert.assertEquals(country1, account1.getCountry());


        account2=accountMapper.selectAccount(userId2);
        Assert.assertEquals(country2, account2.getCountry());



    }




}
