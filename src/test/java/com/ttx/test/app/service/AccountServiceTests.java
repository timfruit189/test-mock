package com.ttx.test.app.service;

import com.ttx.test.app.entity.Account;
import com.ttx.test.app.instance.AccountFactory;
import com.ttx.test.app.mapper.AccountMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.ttx.test.app.instance.AccountFactory.*;
import static org.mockito.Mockito.*;

/**
 * @author TimFruit
 * @date 19-11-17 上午12:13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional  //加事务, 单个方法测试完之后,回滚事务, 各个测试方法互不影响
public class AccountServiceTests {
//    @MockBean
    @SpyBean
    AccountMapper accountMapper;

    @SpyBean
    AccountService accountService;




    @Test
    public void shouldSelectAccount(){

        Account  mockAccount=createTimFruitAccount();

        //打桩
        doReturn(mockAccount)
                .when(accountMapper)
                .selectAccount(timfruitUserId);


        //测试service方法

        Account result=accountService.selectAccount(timfruitUserId);


        //验证
        Assert.assertEquals(mockAccount, result);



    }





    @Test
    public void shouldselectAccountReal(){


        //使用测试数据库中的数据
        // 测试service方法

        Account result=accountService.selectAccount("ttx");


        //验证
        Assert.assertEquals("ttx@yourdomain.com", result.getEmail());

    }



}
