package com.ttx.test.app.instance;

import com.ttx.test.app.entity.Account;
import com.ttx.test.app.mapper.AccountMapper;

/**
 * 统一创建公共的测试实例数据
 * @author TimFruit
 * @date 19-11-17 下午5:55
 */
public class AccountFactory {

    public static final String timfruitUserId="timfruit";

    public static Account createTimFruitAccount(){
        Account mockAccount=new Account();
        mockAccount.setUserid("timfruit");
        mockAccount.setEmail("timfruit@mydomain.com");
        mockAccount.setFirstname("tim");
        mockAccount.setLastname("fruit");
        mockAccount.setCity("天空之城");
        mockAccount.setAddr1("暗黑大陆");
        mockAccount.setAddr2("无空大陆");
        mockAccount.setState("CA");
        mockAccount.setZip("94303");
        mockAccount.setCountry("理想之乡");
        mockAccount.setPhone("88888888");
        return mockAccount;
    }


}
