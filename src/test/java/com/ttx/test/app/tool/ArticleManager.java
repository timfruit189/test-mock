package com.ttx.test.app.tool;

import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @author TimFruit
 * @date 19-11-16 上午10:07
 */
public class ArticleManager {


    private ArticleDataBase articleDataBase;

    public ArticleManager() {
    }

    public ArticleManager(ArticleDataBase articleDataBase) {
        this.articleDataBase = articleDataBase;
    }


    /**
     * 计算id对应的文章的字符数
     * @param id
     * @return
     */
    public Integer calculateArticleWords(int id){
        String article=articleDataBase.getArticleById(id);
        if(article==null){
            return 0;
        }
        // 字数不包括标点；字符数（不计空格）包括标点，不包括空格；字符数（计空格）既包括标点，也包括空格。

        // 这里为简单演示, 使用字符数来算, 即空格不算
        Integer count=0;
        char[] chars=article.toCharArray();
        for(int i=0;i<chars.length;i++){
            if(chars[i]==' '){
                continue;
            }
            count++;
        }
        return count;

    }



    public ArticleDataBase getArticleDataBase(){
        return articleDataBase;
    }


}
