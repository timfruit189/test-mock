package com.ttx.test.app.tool;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;
import org.mockito.exceptions.misusing.NotAMockException;
import org.mockito.exceptions.verification.NoInteractionsWanted;
import org.mockito.exceptions.verification.SmartNullPointerException;
import org.mockito.exceptions.verification.TooFewActualInvocations;
import org.mockito.exceptions.verification.WantedButNotInvoked;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.sound.sampled.Line;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*; //导入所有静态方法

/**
 * 学习mockito工具
 *  https://github.com/mockito/mockito
 * @author TimFruit
 * @date 19-11-10 下午6:49
 */

public class MockitoTests {

    @Test
    public void verifyBehaviour(){

        //创建mock对象 - 接口
        List mockedList=mock(List.class);


        //使用mock对象 模拟调用业务方法
        mockedList.add("one");
        mockedList.clear();


        //验证调用了哪些方法
        verify(mockedList).add("one"); //设定预期调用添加了"one"
        verify(mockedList).clear();


        /*
        mock对象一旦被创建, 它将会记住所有调用过程.然后你可以选择验证你需要的调用过程
         */
    }



    @Test(expected = org.mockito.exceptions.verification.junit.ArgumentsAreDifferent.class)
    public void verifyBehaviourException(){

        //创建mock对象 - 接口
        List mockedList=mock(List.class);


        //使用mock对象 模拟调用业务方法
        mockedList.add("one");
        mockedList.clear();


        //验证调用了哪些方法
        verify(mockedList).add("two");  //设定预期添加"two", 由于实际上是调用添加"one", 不符合预期, 会抛出异常


        /*
        mock对象一旦被创建, 它将会记住所有调用过程.然后你可以选择验证你需要的调用过程
         */
    }



    @Test
    public void stubbing(){
        //打桩, 调用某方法固定返回某些值

        //mock具体类
        LinkedList mockedList=mock(LinkedList.class);

        //stubbing 打桩 类似初始化拥有具体数据的对象

        // 调用get(0)方法, 将会返回"first"
        when(mockedList.get(0)).thenReturn("first");
        // 调用get(1)方法, 将会返回"two"
        when(mockedList.get(1)).thenReturn("two");



        assertEquals("first", mockedList.get(0));
        assertEquals("two", mockedList.get(1));
        // 因为get(999)没有打桩, 所以返回null
        assertEquals(null, mockedList.get(999));


    }



    private ArgumentMatcher<Integer> isValid(){
        return (i) -> {
            if(i==1){
                return true;
            }
            return false;
        }; //自定义参数匹配器
    }


    @Test
    public void argumentMatchers(){
        //参数匹配

        //mock具体类
        LinkedList<String> mockedList=mock(LinkedList.class);


      //stubbing using built-in anyInt() argument matcher
        when(mockedList.get(anyInt())).thenReturn("element");

        //stubbing using custom matcher (let's say isValid() returns your own matcher implementation):
        when(mockedList.contains(argThat(isValid()))).thenReturn(true);

        //following prints "element"
        System.out.println(mockedList.get(999));
        mockedList.add("aaaaaaa");

        //you can also verify using an argument matcher
        verify(mockedList).get(anyInt());

        //argument matchers can also be written as Java 8 Lambdas
        verify(mockedList).add(argThat(someString -> someString.length() > 5));


        /*
        多参数匹配
        verify(mock).someMethod(anyInt(), anyString(), eq("third argument"));
       //above is correct - eq() is also an argument matcher
         */
    }



    @Test
    public void verifyNumberOfInvocation(){
        //验证调用次数

        //mock具体类
        LinkedList<String> mockedList=mock(LinkedList.class);

        //using mock
        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

        //exact number of invocations verification
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockedList, never()).add("never happened");

        //verification using atLeast()/atMost()
        verify(mockedList, atMostOnce()).add("once");
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(2)).add("three times");
        verify(mockedList, atMost(5)).add("three times");



    }


    @Test(expected = RuntimeException.class)
    public void stubbingMethodWithException(){
        //预设调用某方法是抛出一样

        //mock具体类
        LinkedList<String> mockedList=mock(LinkedList.class);

        doThrow(new RuntimeException()).when(mockedList).clear();



        //调用时将会抛出异常 RuntimeException
        mockedList.clear();

        // Read more about doThrow()|doAnswer() family of methods in section 12.
    }



    @Test
    public void verifyInOrder(){
        //验证调用顺序
        // A. Single mock whose methods must be invoked in a particular order
        List singleMock = mock(List.class);

        //using a single mock
        singleMock.add("was added first");
        singleMock.add("was added second");

        //create an inOrder verifier for a single mock
        InOrder inOrder = inOrder(singleMock);

        //following will make sure that add is first called with "was added first", then with "was added second"
        inOrder.verify(singleMock).add("was added first");
        inOrder.verify(singleMock).add("was added second");

        // B. Multiple mocks that must be used in a particular order
        List firstMock = mock(List.class);
        List secondMock = mock(List.class);

        //using mocks
        firstMock.add("was called first");
        secondMock.add("was called second");

        //create inOrder object passing any mocks that need to be verified in order
        InOrder inOrderDevide = inOrder(firstMock, secondMock);

        //following will make sure that firstMock was called before secondMock
        inOrderDevide.verify(firstMock).add("was called first");
        inOrderDevide.verify(secondMock).add("was called second");

    }



    @Test(expected = NoInteractionsWanted.class)
    public void redundantInvocation(){
        //验证多余的步骤
        List mockedList = mock(List.class);

        //using mocks
        mockedList.add("one");
        mockedList.add("two");

        verify(mockedList).add("one");

        //following verification will fail
        verifyNoMoreInteractions(mockedList);
    }





    // -------------------------------------
    // shorthandMockCreation 注解mock

    //mock注解需要使用 @RunWith(MockitoJUnitRunner.class) 或者 MockitoAnnotations.initMocks()
    @Mock
    private ArticleDataBase articleDataBase;
    private ArticleManager articleManager;

    @Before
    public void init(){
        //mock注解属性
        MockitoAnnotations.initMocks(this);

        articleManager=new ArticleManager(articleDataBase);
    }


    //这个算是比较正式的单元测试了
    @Test
    public void shorthandMockCreation(){

        // stubbing 预设返回值
        when(articleDataBase.getArticleById(1))
                .thenReturn("a b c d 是中国, 古里无今算什么");

        // 调用需要测试的方法
        int length=articleManager.calculateArticleWords(1);

        // 判断结果
        assertEquals(15, length);

    }

    // ---------------------------


    @Test
    public void stubbingConsecutiveCalls(){
        //连续调用某个方法,返回不同的值
        List<String> mockedList = mock(List.class);

        when(mockedList.get(0))
                .thenReturn("one")
                .thenReturn("two");

        assertEquals("one", mockedList.get(0));
        assertEquals("two", mockedList.get(0));
        assertEquals("two", mockedList.get(0));

    }


    @Test
    public void stubbingWithCallbacks(){
        //打桩时, 使用回调返回值

        List<String> mockedList = mock(List.class);
        when(mockedList.get(0))
                .thenReturn("one");


        when(mockedList.get(0)).thenAnswer(
                new Answer() {
                    public Object answer(InvocationOnMock invocation) {
                        Object[] args = invocation.getArguments();
                        Object mock = invocation.getMock();
                        return "called with arguments: " + Arrays.toString(args);
                    }
                });

        assertEquals("called with arguments: [0]", mockedList.get(0));

    }



    /*
    doReturn(Object)
    doThrow(Throwable...)
    doThrow(Class)
    doAnswer(Answer)
    doNothing()
    doCallRealMethod()
     */


    @Test
    public void doMethod(){
        List<String> mockedList = mock(List.class);
        when(mockedList.get(0))
                .thenReturn("one");

        assertEquals("one", mockedList.get(0));
        assertEquals("one", mockedList.get(0));
        assertEquals("one", mockedList.get(0));
        assertEquals("one", mockedList.get(0));


        //doReturn适用于重新打桩, 适用于spy
        doReturn("other")
                .when(mockedList)
                .get(0);

        assertEquals("other", mockedList.get(0));


    }



    // ===========================================================
    // spy
    // ===========================================================
    public void spyingOnRealObjects(){
        //观察真实对象

        List list = new LinkedList();


        List spy = spy(list);

        //optionally, you can stub out some methods:
        when(spy.size()).thenReturn(100);

        //调用真实方法
        //using the spy calls *real* methods
        spy.add("one");
        spy.add("two");

        //prints "one" - the first element of a list
        System.out.println(spy.get(0));

        //size() method was stubbed - 100 is printed
        System.out.println(spy.size());

        //optionally, you can verify
        verify(spy).add("one");
        verify(spy).add("two");
    }


    @Test
    public void stubbingOnSpy(){
        //对spy对象打桩

        List list = new LinkedList();
        List spy = spy(list);

        //You have to use doReturn() for stubbing
        doReturn("foo").when(spy).get(0);

        //这个会调用真实方法,所以抛出异常
//        //Impossible: real method is called so spy.get(0) throws IndexOutOfBoundsException (the list is yet empty)
//        when(spy.get(0)).thenReturn("foo");

        //注意, 不能对final方法打桩
        // when you spy on real objects + you try to stub a final method = trouble

    }


    @Test(expected = SmartNullPointerException.class)
    public void changeDefaultReturnValuesOfUnstubbedInvocations(){

        ArticleManager mock = mock(ArticleManager.class, Mockito.RETURNS_SMART_NULLS);

        //这里返回null
        ArticleDataBase dataBase=mock.getArticleDataBase();

        // 抛出SmartNullPointerException, 指出哪里有空指针异常
        dataBase.getArticleById(1);

    }


    @Test
    public void capturingArgumentsForFurtherAssertions(){
        ArticleManager mock=mock(ArticleManager.class);

        when(mock.calculateArticleWords(9)).thenReturn(9);


        //调用测试方法
        mock.calculateArticleWords(9);

        //验证调用参数
        ArgumentCaptor<Integer> argument = ArgumentCaptor.forClass(Integer.class);
        verify(mock).calculateArticleWords(argument.capture());
        assertEquals(9, argument.getValue().intValue());
    }


    @Test
    public void realPartialMocks(){

        // 1. 使用spy使用真实对象方法
       //you can create partial mock with spy() method:
        List list = spy(new LinkedList());


        //2. thenCallRealMethod方法调用真实方法
        //you can enable partial mock capabilities selectively on mocks:
        ArticleManager mock = mock(ArticleManager.class);
        //Be sure the real implementation is 'safe'.
        //If real implementation throws exceptions or depends on specific state of the object then you're in trouble.
        when(mock.getArticleDataBase()).thenCallRealMethod();
    }


    @Test
    public void resettingMocks(){
        List mock = mock(List.class);
        when(mock.size()).thenReturn(10);
        mock.add(1);

        reset(mock);
        //at this point the mock forgot any interactions & stubbing
    }



    @Test
    public void serializableMocks(){
        //创建可以序列化的mock
        List serializableMock = mock(List.class, withSettings().serializable());

        //spy
        List<Object> list = new ArrayList<Object>();
        List<Object> spy = mock(ArrayList.class, withSettings()
                .spiedInstance(list)
                .defaultAnswer(CALLS_REAL_METHODS)
                .serializable());
    }


    @Test(expected = WantedButNotInvoked.class)
    public void verificationWithTimeout(){
        //验证超时

        List mock = mock(List.class);


        //passes when someMethod() is called no later than within 100 ms
        //exits immediately when verification is satisfied (e.g. may not wait full 100 ms)
        verify(mock, timeout(100)).size();
        //above is an alias to:
        verify(mock, timeout(100).times(1)).size();

        //passes as soon as someMethod() has been called 2 times under 100 ms
        verify(mock, timeout(100).times(2)).size();

        //equivalent: this also passes as soon as someMethod() has been called 2 times under 100 ms
        verify(mock, timeout(100).atLeast(2)).size();
    }


    // -----------------------------------------


    // 在mock时打桩
    // Car boringStubbedCar = when(mock(Car.class).shiftGear()).thenThrow(EngineNotStarted.class).getMock();



    // -----------------------------------------
    class Lines extends ArrayList<Line> {
        // ...
    }
    public void betterGenericSupportWithDeepStubs(){

        Lines lines = mock(Lines.class, RETURNS_DEEP_STUBS);

        // Now Mockito understand this is not an Object but a Line
        Line line = lines.iterator().next();
    }




}
