package com.ttx.test.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author TimFruit
 * @date 19-11-10 下午2:09
 */
@SpringBootApplication
public class MockApp {
    public static void main(String[] args) {
        SpringApplication.run(MockApp.class, args);
    }
}
