package com.ttx.test.app.service;

import com.ttx.test.app.entity.Account;
import com.ttx.test.app.mapper.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author TimFruit
 * @date 19-11-17 上午12:12
 */
@Service
public class AccountService {


    @Autowired
    AccountMapper accountMapper;


    public Account selectAccount(String userid){
        return accountMapper.selectAccount(userid);
    }

}
