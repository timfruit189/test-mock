package com.ttx.test.app.mapper;

import com.ttx.test.app.entity.Account;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author TimFruit
 * @date 19-11-17 上午12:02
 */
public interface AccountMapper {

    /**
     * 根据用户id(用户名)查找账号
     * @param userid
     * @return
     */
    Account selectAccount(@Param("userid") String userid);

    /**
     * 插入账号
     * @param account
     * @return
     */
    void insertAccount(Account account);


    void updateCountryByUserId(@Param("userid") String userid,
                       @Param("country") String country);


    void deleteAccountry(@Param("userid") String userid);

    /**
     * 批量插入或更新
     * @param accounts
     */
    void saveAccountBatch(@Param("accounts")List<Account> accounts);

}
